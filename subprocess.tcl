# Copyright © 2015, Moritz Wilhelmy <mw plus eggdrop at barfooze dot de>
# All rights reserved.
# 
# This project is licensed under the "tell the author you use it" license, which
# is essentially a modified 2-clause BSD license:
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# 1. You are encouraged to send me an email to the above address if you use this
#    module, even if the mail is about how much you love pancakes rather than the
#    project. Say hi. Really, that's all I want from you.
# 
# 2. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
# 
# 3. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# 
if {[catch {package require stooop}]} {
	error "This package requires stooop which is part of tcllib"
}

namespace eval ::subprocess {

if {[catch {package require Tclx 8.0}]} {
	set have_tclx 0

	proc _kill {pid {sig TERM}} {
		# exec does not leave a zombie process so we're good
		exec kill -$sig $pid
	}
	proc _pipe {PR PW} {
		upvar $PR pr $PW pw
		lassign [chan pipe] pr pw
	}
	# no solution for zombie processes :(
	# HACK maybe patch eggdrop to ignore SIGCHLD? it's only used while
	# forking to background.
} else {
	set have_tclx 1

	proc _pipe {pr pw} {
		uplevel ::pipe $pr $pw
	}
	proc _kill {pid {sig TERM}} {
		::kill $sig $pid
	}
}
if {!$have_tclx && [catch {package require Tcl 8.6}]} {
	error "Either Tcl >= 8.6.0 or Tclx >= 8.4 are required. Bailing out. If you can, get both, but preferably Tclx. Tclx 8.4 is fine even with newer Tcl versions"
}

variable ns [namespace current]

# I don't want to rely on {*} so this will hopefully work even with 8.4
proc new {args} {
	eval [linsert $args 0 ::stooop::new ::subprocess]
}

# This class can hopefully be easily subclassed.
::stooop::class ::subprocess {
	# To redirect stderr to a file descriptor, make it start with @.
	# e.g. to redirect stderr to stdout, use @1
	# Capturing stderr as part of this module is not yet supported.
	proc subprocess {this command args} {
		set default {
			-readCallback ""
			-writeCallback ""
			-stderr /dev/null
			-buffering line
			-mode rw
		}
		
		set valid {}
		foreach {k v} $default { set ($this,$k) $v; lappend valid $k }
		foreach {k v} $args {
			if {[lsearch -exact $valid $k] == -1} {
				error "invalid parameter $k, must be $valid"
			}
			set ($this,$k) $v
		}

		set ($this,status) ""
		set ($this,piper) ""
		set ($this,pipew) ""
		set ($this,pid) ""

		set mode $($this,-mode)
		set stderr $($this,-stderr)

		if {$mode == "r" || $mode == "w"} {
			# You could actually just use "open" in that case.
			set ($this,pipe$mode) [open "|$command 2>$stderr" $mode]
			set ($this,pid) [pid $($this,pipe$mode)]
		} elseif {$mode == "rw"} {
			# Bidirectional pipe
			::subprocess::_pipe pr pw
			set ($this,piper) [open "|$command <@ $pr 2>$stderr" r]
			set ($this,pipew) $pw
			set ($this,pid) [pid $($this,piper)]
		} else {
			error "Unknown mode: $mode"
		}
		set ($this,status) "running"

		# FIXME better setup for -buffering
		if {$($this,pipew) != ""} {
			fconfigure $($this,pipew) -blocking 0 -buffering $($this,-buffering)
		}
		if {$($this,piper) != ""} {
			fconfigure $($this,piper) -blocking 0 -buffering $($this,-buffering)
		}

		readCallback $this $($this,-readCallback)
		writeCallback $this $($this,-writeCallback)
	}
	proc ~subprocess {this} {
		terminate $this
	}
	proc terminate {this {sig TERM} {-nohang 0}} {
		catch {close_write $this}
		catch {close_read $this}
		catch {
			kill $this $sig
			if {${-nohang} != 0} {
				::wait -nohang $pid
			} else {
				::wait $pid ;# try to reap zombie
				# XXX ^if this causes problems (hangs) the way
				# you use this module, please report a bug.
			}
		}
	}
	# write to the pipe. Can be invoked via writeCallback, which is invoked
	# from "fileeveent writable". This means the module sits completely
	# between the pipe and Tcl scripts.
	proc put {this data} {
		if {$($this,pipew) == ""} {
			error "ceci n'est pas une pipe"
		}
		if {[catch {puts -nonewline $($this,pipew) $data}] ||
		    [catch {flush $($this,pipew)}]
		} then {
			if {[eof $($this,pipew)]} {
				close_write $this
			}
			return eof ;# FIXME?
		}
		return 1
	}
	# syntax sugar
	proc putline {this data} {
		put $this "$data\n"
	}
	# helper that invokes the readCallback
	proc invokeCallback {this args} {
		if {$($this,-readCallback) != ""} {
			eval [linsert $args 0 $($this,-readCallback) $this]
		}
	}
	# Handler for incoming data from the pipe.
	# Called via fileevent from the constructor
	proc get {this} {
		if {$($this,-buffering) != "line"} {
			error "subprocess: non-line-buffered pipes are not yet implemented"
		}
		if {$($this,piper) == ""} {
			error "pipe not open for reading. This shouldn't happen. Please report a bug."
		}
		if {[catch {gets $($this,piper) line} err] || [eof $($this,piper)]} {
			set rc ""
			catch {set rc [::wait -nohang $($this,pid)]}

			if {$rc == ""} {
				# process closed its file descriptor
				# let the callback decide what to do
				invokeCallback $this eof $($this,piper)
			} else {
				invokeCallback $this terminated $rc
				set ($this,status) terminated
			}
			close_read $this

			return
		}
		invokeCallback $this data $line
		return
	}
	# Close the read end of the pipe
	proc close_read {this} {
		catch {close $($this,piper)}
		set ($this,piper) ""
	}
	# Close the write end of the pipe
	# Could be useful for pipes that expect EOF on stdin
	proc close_write {this} {
		catch {close $($this,pipew)}
		set ($this,pipew) ""
	}
	proc status {this} {
		return $($this,status)
	}
	proc kill {this sig} {
		::subprocess::_kill $($this,pid) $sig
	}

	# 0 args: get current write callback
	# 1 arg:  set current write callback
	proc writeCallback {this args} {
		if {[llength $args] == 0} {
			return $($this,-writeCallback)
		} elseif {[llength $args] == 1} {
			set ($this,-writeCallback) [lindex $args 0]
			if {$($this,-writeCallback) != "" && $($this,pipew) != ""} {
				fileevent $($this,pipew) writable [list $($this,-writeCallback) $this]
			}
		} else {
			error "invalid number of parameters"
		}
	}
	proc readCallback {this args} {
		if {[llength $args] == 0} {
			return $($this,-readCallback)
		} elseif {[llength $args] == 1} {
			set ($this,-readCallback) [lindex $args 0]
			if {$($this,-readCallback) != "" && $($this,piper) != ""} {
				fileevent $($this,piper) readable [list ::subprocess::get $this]
			}
		} else {
			error "invalid number of parameters"
		}
	}
}

} ;# end namespace

# TODO
# - possibly call ::wait in a different thread that signals the main thread in case a child process dies.
